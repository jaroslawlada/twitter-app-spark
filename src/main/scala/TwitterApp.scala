import analysers.{TweetsAnalyzer, TweetsSearch}
import cleaners.TweetsCleaner
import loaders.TweetsLoader
import org.apache.spark.sql.functions.{col, regexp_extract, regexp_replace}
import org.apache.spark.sql.{Dataset, Row, SparkSession}

object TwitterApp {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder()
      .appName("twitter-app")
      .master("local[*]")
      .getOrCreate()

    val tweetsLoader: TweetsLoader = new TweetsLoader(spark)
    val tweetsCleaner: TweetsCleaner = new TweetsCleaner(spark)
    val tweetsSearch: TweetsSearch = new TweetsSearch(spark)
    val tweetsAnalyzer: TweetsAnalyzer = new TweetsAnalyzer(spark)

    import tweetsSearch._

    val tweetDF: Dataset[Row] = tweetsLoader.loadAllTweets().cache()
    val tweetsCleanedDF: Dataset[Row] = tweetsCleaner.cleanAllTweets(tweetDF)

    val trumpTweetsDF: Dataset[Row] = tweetsCleanedDF.transform(searchByKeyWord("Trump"))
      .transform(onlyInLocation("United States"))

    val sourceCount: Dataset[Row] = tweetsAnalyzer.calculateSourceCount(trumpTweetsDF)
    sourceCount.show()
  }
}
