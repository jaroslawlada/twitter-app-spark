package analysers

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Row, SparkSession}

object TweetsAnalyzer {
  private val HASHTAG_COLUMN: String = "hashtags"
  private val IS_RETWEET_COLUMN: String = "is_retweet"
  private val SOURCE_COLUMN: String = "source"
  private val USER_FOLLOWERS: String = "user_followers"
  private val USER_NAME: String = "user_name"
  private val USER_LOCATION: String = "user_location"
}

class TweetsAnalyzer(sparkSession: SparkSession) {

  /**
   * AGGREGATION
   * @param df
   * @return Dataframe with columns hashtag, count
   */
  def calculationHashtag(df: Dataset[Row]): Dataset[Row] = {
    df.withColumn(TweetsAnalyzer.HASHTAG_COLUMN, explode_outer(col(TweetsAnalyzer.HASHTAG_COLUMN)))
      .groupBy(TweetsAnalyzer.HASHTAG_COLUMN).count()
  }

  /**
   * AGGREGATION
   *
   * @param df
   * @return Dataframe with columns is_retweet, count
   */
  def calculateIsRetweetCount(df: Dataset[Row]): Dataset[Row] = {
    df.groupBy(TweetsAnalyzer.IS_RETWEET_COLUMN).count()
  }

  /**
   * AGGREGATION
   *
   * @param df
   * @return Dataframe with columns source, count
   */
  def calculateSourceCount(df: Dataset[Row]): Dataset[Row] = {
    df.groupBy(TweetsAnalyzer.SOURCE_COLUMN).count()
  }

  /**
   * AGGREGATION
   *
   * @param df
   * @return Dataframe with column user_location, ang
   */
  def calculateAvgUserFollowersPerLocation(df: Dataset[Row]): Dataset[Row] = {
    df.select(TweetsAnalyzer.USER_NAME, TweetsAnalyzer.USER_FOLLOWERS, TweetsAnalyzer.USER_FOLLOWERS)
      .filter(col(TweetsAnalyzer.USER_NAME).isNotNull)
      .filter(col(TweetsAnalyzer.USER_LOCATION).isNotNull)
      .dropDuplicates(TweetsAnalyzer.USER_NAME)
      .groupBy(TweetsAnalyzer.USER_LOCATION)
      .avg(TweetsAnalyzer.USER_FOLLOWERS)
      .as("avg")
  }

}
